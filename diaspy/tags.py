#!/usr/bin/env python3
from diaspy.models import Tag
from diaspy import errors
class Tags():
	def __init__(self, connection):
		self._connection = connection
		self._tags = self.get()

		"""
			id
			name
			taggings_count
		"""

	def __iter__(self):
		return iter(self._tags)

	def __getitem__(self, t):
		return self._tags[t]

	def _finalise(self, tags):
		return [Tag(self._connection, t['id'], t['name'], t['taggings_count']) for t in tags]

	def update(self):
		self._tags = self.get()

	def follow(self, name):
		data = {'authenticity_token': repr(self._connection)}
		params = {'name': name}
		request = self._connection.post('tag_followings', data=data, params=params, headers={'accept': 'application/json'})
		if request.status_code != 201:
			raise errors.TagError('{0}: Tag could not be followed.'.format(request.status_code))

	def get(self):
		request = self._connection.get('tag_followings.json')
		if request.status_code != 200:
			raise Exception('status code: {0}: cannot retreive tag_followings'.format(request.status_code))
		return self._finalise(request.json())
