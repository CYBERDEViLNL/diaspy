import json
from diaspy.models import Post, Aspect
from diaspy import errors

class Publisher():
    def __init__(self, connection):
        """
        :param connection: Connection() object
        :type connection: diaspy.connection.Connection
        """
        self._connection = connection

    def post(self, text='', aspect_ids='public', photos=None, photo='', poll_question=None, poll_answers=None, location_coords=None, provider_display_name=''):
        """This function sends a post to an aspect.
        If both `photo` and `photos` are specified `photos` takes precedence.

        :param text: Text to post.
        :type text: str
        
        :param aspect_ids: Aspect ids to send post to.
        :type aspect_ids: str
        
        :param photo: filename of photo to post
        :type photo: str
        
        :param photos: id of photo to post (obtained from _photoupload())
        :type photos: int
        
        :param provider_display_name: name of provider displayed under the post
        :type provider_display_name: str

        :param poll_question: Question string
        :type poll_question: str

        :param poll_answers: Anwsers to the poll
        :type poll_answers: list with strings

        :param location_coords: TODO
        :type location_coords: TODO

        :returns: diaspy.models.Post -- the Post which has been created


        --> POST /status_messages HTTP/1.1
            {"status_message":{"text":""},"aspect_ids":"29884","photos":["822346","822348","822349"],"location_coords":"","poll_question":"Test","poll_answers":["Da","No"]}

        <-- HTTP/1.1 201 Created
            
        """
        data = {}
        data['aspect_ids'] = aspect_ids
        data['status_message'] = {'text': text, 'provider_display_name': provider_display_name}
        if photo: data['photos'] = self._photoupload(photo)
        if photos: data['photos'] = photos
        if poll_question and poll_answers:
            data['poll_question'] = poll_question
            data['poll_answers'] = poll_answers
        if location_coords: data['location_coords'] = location_coords
    
        request = self._connection.post('status_messages',
                                        data=json.dumps(data),
                                        headers={'content-type': 'application/json',
                                                 'accept': 'application/json',
                                                 'x-csrf-token': repr(self._connection)})
        if request.status_code != 201:
            raise Exception('{0}: Post could not be posted.'.format(request.status_code))
        post_json = request.json()
        post = Post(self._connection, id=post_json['id'], guid=post_json['guid'], post_data=post_json)
        return post

    def _deletephoto(self, photo_id):
        """
            --> DELETE /photos/{PHOTO_ID} HTTP/1.1
            <-- HTTP/1.1 204 No Content
        """
        request = self._connection.delete('/photos/{0}'.format(photo_id))
        if request.status_code != 204:
            raise Exception('{0}: Could not delete photo id: {1}.'.format(request.status_code, photo_id))

    def _photoupload(self, filename, aspects=[], set_profile_picture=False, pending='true'):
        """Uploads picture to the pod.

        :param filename: path to picture file
        :type filename: str
        :param aspect_ids: list of ids of aspects to which you want to upload this photo
        :type aspect_ids: list of integers

        :returns: id of the photo being uploaded

        Request:
            POST /photos HTTP/1.1
            Accept: application/json
            Accept-Encoding: gzip, deflate, br
            X-Requested-With: XMLHttpRequest
            Cache-Control: no-cache
            Content-Length: 135625
            Content-Type: multipart/form-data; boundary=---------------------------14766238631967041419555645811
            Cookie: _diaspora_session={SESSION}; remember_user_token={TOKEN}
            Connection: keep-alive
            Pragma: no-cache
            -----------------------------14766238631967041419555645811
            Content-Disposition: form-data; name="authenticity_token"

            {TOKEN}
            -----------------------------14766238631967041419555645811
            Content-Disposition: form-data; name="photo[pending]"

            true
            -----------------------------14766238631967041419555645811
            Content-Disposition: form-data; name="qquuid"

            d53d1903-fa21-47a1-a3d0-a7c03c3b7675
            -----------------------------14766238631967041419555645811
            Content-Disposition: form-data; name="qqfilename"

            test.jpg
            -----------------------------14766238631967041419555645811
            Content-Disposition: form-data; name="qqtotalfilesize"

            134563
            -----------------------------14766238631967041419555645811
            Content-Disposition: form-data; name="qqfile"; filename="test.jpg"
            Content-Type: image/jpeg

            ÿØÿá

        Response:
            HTTP/1.1 200 OK
            Transfer-Encoding: chunked


        Supported mime types:
            JPG        `image/jpeg`
            JPEG       `image/jpeg`
            PNG        `image/png`
            GIF        `image/gif`

        sizeLimit = 4194304; // eehh, this doesn't seem to matter I just uploaded a 11MB photo.

        Sources:
            https://github.com/diaspora/diaspora/blob/develop/app/controllers/photos_controller.rb
        """
        data = open(filename, 'rb')
        image = data.read()
        data.close()

        tmp = filename.split('/')
        filename = tmp[len(tmp)-1]
        print(filename)

        data = {}
        data['photo[pending]'] = pending

        # Only set if needed
        if set_profile_picture: data['photo[set_profile_photo]'] = 'true'

        data['qqfilename'] = filename
        data['authenticity_token'] = repr(self._connection)

        files = {'qqfile': (filename, image, 'image/jpeg')}
        
        if not aspects: aspects = self._connection.getUserData()['aspects']
        for i, aspect in enumerate(aspects):
            aspect_type = type(aspect)
            if aspect_type == int:
                data['photo[aspect_ids][{0}]'.format(i)] = aspect
            elif aspect_type == dict:
                data['photo[aspect_ids][{0}]'.format(i)] = aspect['id']

        request = self._connection.post('photos', data=data, files=files)
        if request.status_code != 200:
            raise errors.StreamError('photo cannot be uploaded: {0}'.format(request.status_code))
        print(request.json()) # DEBUG
        return request.json()['data']['photo']['id']
